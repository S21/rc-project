class ValueList < Sinatra::Base
  get '/values' do
    'Hello, World!'
    # @values = Value.all
    # erb :"values/index"
  end

  post '/values' do
    Value.create(name: params[:name])
    redirect '/values'
  end
end
