class Value < ActiveRecord::Base
  validates :name, presence: true
end
