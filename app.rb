require 'sinatra'
require 'sinatra/activerecord'

class YourApplication < Sinatra::Base
  register Sinatra::ActiveRecordExtension
end

get '/' do
  'Your Sinatra app works!'
end

get '/values' do
  'Hello, World!'
end

post '/values' do
  Value.create(name: params[:name])
  redirect '/values'
end
