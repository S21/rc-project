# RC-project

## Database-server

The programme runs a server that is accessible on <http://localhost:4000/>. When the server receives a request on <http://localhost:4000/set?somekey=somevalue> it should store the passed key and value in memory. When it receives a request on <http://localhost:4000/get?key=somekey> it should return the value stored at somekey.

## Getting started

- Clone the repository using this command:

```markdown
git clone git@codeberg.org:S21/rc-project.git
```

You need to have Ruby / RBenv / Bundler installed already.

- Install the Gems with

```markdown
bundle install
```

## Start the PostgreQSL server

```markdown
brew services start postgresql@14
```

If you need to stop the PostgreSQL server, use the command below:

```markdown
brew services stop postgresql@14
```

## Launch the Sinatra server

Open the terminal and run the command:

```markdown
ruby app.rb -p 4000
```

It will listen on port 4000.
